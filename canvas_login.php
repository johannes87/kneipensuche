<!DOCTYPE html>
<html>
<head>
<title>Facebook Login PHP Example</title>
<meta charset="UTF-8">
</head>
<body>

<?php

require('vendor/autoload.php');
require('common.php');

use Facebook\FacebookSession;
use Facebook\FacebookCanvasLoginHelper;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\GraphObject;
use Facebook\GraphUser;

session_start();

FacebookSession::setDefaultApplication('665872606799892', '18ab8aa41a03142aad47613b90ed9a8b');

$helper = new FacebookCanvasLoginHelper();

$session = $helper->getSession();

if (!$session) {
    $redirect_helper = new FacebookRedirectLoginHelper('https://apps.facebook.com/665872606799892');
    $loginUrl = $redirect_helper->getLoginUrl();
?>
<script>parent.window.location = "<? echo $loginUrl ?>"</script>
<?
} else {
    try {
        $response = (new FacebookRequest($session, 'GET', '/me'))->execute();
        $object = $response->getGraphObject();
        $name = $object->getProperty('name');
        
        echo $twig->render('canvas.html.twig', array('name' => $name));
    } catch (FacebookRequestException $ex) {
        echo $ex->getMessage();
    } catch (\Exception $ex) {
        echo $ex->getMessage();
    }
}

?>

</body>
</html>
