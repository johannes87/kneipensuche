<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

class Common {

    private static $facebook_app_id = "665872606799892";
    private static $facebook_app_secret = "18ab8aa41a03142aad47613b90ed9a8b";

    private static $mysql_user = "kneipensuche";
    private static $mysql_password = "Aong7aire7";
    private static $mysql_db = "kneipensuche";

    public static function db_init()
    {
        $db = new PDO('mysql:host=localhost;dbname='.self::$mysql_db.';charset=utf8', self::$mysql_user, self::$mysql_password);
        return $db;
    }

    public static function twig_init()
    {
        $loader = new Twig_Loader_Filesystem('templates');
        $twig = new Twig_Environment($loader);
        $twig->addExtension(new Twig_Extensions_Extension_Text());
        return $twig;
    }

    public static function facebook_get_app_url() { 
        return "https://apps.facebook.com/" . self::$facebook_app_id;
    }

    public static function facebook_session_init() {

        FacebookSession::setDefaultApplication(self::$facebook_app_id, self::$facebook_app_secret);

    }

    public static function facebook_get_object($session, $path) {
        $response = (new FacebookRequest($session, 'GET', $path))->execute();
        return $response->getGraphObject();
    }
    
    public static function pp_html($arr){
        $retStr = '<ul>';
        if (is_array($arr)){
            foreach ($arr as $key=>$val){
                if (is_array($val)){
                    $retStr .= '<li>' . $key . ' => ' . pp($val) . '</li>';
                }else{
                    $retStr .= '<li>' . $key . ' => ' . $val . '</li>';
                }
            }
        }
        $retStr .= '</ul>';
        return $retStr;
    }

}

$twig = Common::twig_init();
$db = Common::db_init();
umask(0002); // so that we can delete the webserver-created files ourselves

?>
