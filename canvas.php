<?php

require('vendor/autoload.php');
require('common.php');


use Facebook\FacebookCanvasLoginHelper;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\GraphObject;
use Facebook\GraphUser;

function download_band_assets($fb_session, $fb_page) {
    $fb_page_id = $fb_page['id'];

    $path_prefix = sprintf("img/fb_data/%d/", $fb_page_id);
    mkdir($path_prefix);

    // fetch the profile image
    $fb_page_picture_url = Common::facebook_get_object($fb_session, sprintf("/%s/picture?redirect=0&type=square&width=200&height=200", $fb_page_id))->asArray()->url;
    file_put_contents($path_prefix."profile.jpg", file_get_contents($fb_page_picture_url));

    $fb_page_albums = Common::facebook_get_object($fb_session, sprintf("/%s/albums", $fb_page_id))->asArray();

    // determine album_id (use the first non-profile album) we want to show
    $fb_album_id;
    foreach ($fb_page_albums['data'] as $album) {
        if ($album->type == "normal") {
            $fb_album_id = $album->id;
            break;
        }
    }
    
    // fetch the photos from the album 
    $fb_album_photos = Common::facebook_get_object($fb_session, sprintf("/%s/photos", $fb_album_id))->asArray();

    for ($photo_nr = 0; $photo_nr < sizeof($fb_album_photos['data']); $photo_nr++) {
        $photo = $fb_album_photos['data'][$photo_nr];
        file_put_contents($path_prefix."gallery_".$photo_nr.".jpg", file_get_contents($photo->source));
    } 
}

session_start();

Common::facebook_session_init();

$helper = new FacebookCanvasLoginHelper();
$fb_session = $helper->getSession();

$account_type = NULL;


if ($fb_session) {
    $fb_me = Common::facebook_get_object($fb_session, '/me')->asArray();
    $fb_me_pages = Common::facebook_get_object($fb_session, '/me/accounts')->asArray();
    $sth = $db->prepare("SELECT type FROM facebook_users WHERE id = ?");
    $sth->execute(array($fb_me['id']));
    $account_type = $sth->fetchColumn();
}

$after_login = isset($_GET['login_bar']) || isset($_GET['login_band']);

// second case is for when a facebook session exists, but the "internal" account 
// hasn't been created yet.
if (!$fb_session || ($fb_session && !$account_type && !$after_login)) {
    $scope = array('manage_pages', 'read_insights');

    $redirect_helper_login_bar = new FacebookRedirectLoginHelper(Common::facebook_get_app_url()."/login_bar");
    $redirect_helper_login_band = new FacebookRedirectLoginHelper(Common::facebook_get_app_url()."/login_band");

    echo $twig->render("canvas.html.twig", 
        array(
            'login_band_link' => $redirect_helper_login_band->getLoginUrl($scope),
            'login_bar_link' => $redirect_helper_login_bar->getLoginUrl($scope)
        )
    );
} else {
    if (!$fb_me_pages['data']) {
        echo $twig->render("error.html.twig",
            array(
                'error_heading' => 'Keine Bar oder Band-Seite gefunden',
                'error_desc' => 'Sie müssen Administrator einer Bar- oder Band-Seite sein, um diese Facebook-Anwendung verwenden zu können.'
            )
        );

        exit();
    }

    $fb_page_id = $fb_me_pages['data'][0]->id;
    $fb_page = Common::facebook_get_object($fb_session, "/".$fb_page_id)->asArray();

    // user chose to login, so we have to create an account now
    if ($fb_session && !$account_type && $after_login) {
        $account_type = isset($_GET['login_bar']) ? 'bar' : 'band';

        
        $sth = $db->prepare(
            "INSERT INTO facebook_users
            (id, facebook_page_id, type)
            VALUES (?, ?, ?)");
        $sth->execute(array($fb_me['id'], $fb_page_id, $account_type));

        if ($account_type == 'bar') {
            $sth = $db->prepare(
                "INSERT INTO bars 
                (facebook_page_id, name, description, addr_city, addr_country, addr_street, addr_zip)
                VALUES (?, ?, ?, ?, ?, ?, ?)");
            $sth->execute(array($fb_page_id, $fb_page['name'], $fb_page['about'], 
                $fb_page['location']->city, $fb_page['location']->country, 
                $fb_page['location']->street, $fb_page['location']->zip));
             
        } else if ($account_type == 'band') {
            $sth = $db->prepare(
                "INSERT INTO bands
                (facebook_page_id, name, description, addr_city, addr_country, addr_street, addr_zip, genre, fb_link, press_contact, general_manager, hometown)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $sth->execute(array($fb_page_id, $fb_page['name'], $fb_page['about'], 
                $fb_page['location']->city, $fb_page['location']->country, 
                $fb_page['location']->street, $fb_page['location']->zip,
                $fb_page['genre'], $fb_page['link'], $fb_page['press_contact'], 
                $fb_page['general_manager'], $fb_page['hometown']
            ));

            download_band_assets($fb_session, $fb_page);
        }
    }

    //echo "<pre>";
    //var_dump(Common::facebook_get_object($fb_session, "/".$fb_page_id));
    //echo "</pre>";
    //exit();

    
    // we have an account at this point, so we show the dashboard
    if ($account_type == 'bar') {

        $band_exists = false; 
        $sth = $db->query("SELECT * FROM bands WHERE facebook_page_id = 1445953342324176");
        $row = $sth->fetch(PDO::FETCH_ASSOC);

        echo $twig->render("bar_dashboard.html.twig", array('band_data' => $row));
    } else if ($account_type == 'band') {
        echo $twig->render("band_dashboard.html.twig");
    }
}



?>
