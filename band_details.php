<?php

require('vendor/autoload.php');
require('common.php');

if (isset($_GET["source"]) && $_GET["source"] == "fb") {
    $sth = $db->query("SELECT * FROM bands WHERE facebook_page_id = 1445953342324176");
    $row = $sth->fetch(PDO::FETCH_ASSOC);

    echo $twig->render("band_details.html.twig", array('band_data' => $row));
} else {
    echo $twig->render("band_details_breakeven.html.twig");
}

?>
